using UnityEngine;

public class PlayerProfile : MonoBehaviour
{
    #region singleton
    /*private static PlayerProfile _instance = null;
    
    public static PlayerProfile Instance
    {
        get {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PlayerProfile>();
             
                if (_instance == null)
                {
                    GameObject container = new GameObject("Bicycle");
                    _instance = container.AddComponent<PlayerProfile>();
                }
            }
     
            return _instance;
        }
    }*/
    #endregion
    public string PlayerName
    {
        get => PlayerPrefs.GetString("PlayerName", "Guest");
        set => PlayerPrefs.SetString("PlayerName", value);
    }
    
    //[Range(-10f,10f)]
    public int PlayerHappiness
    {
        get => PlayerPrefs.GetInt("Happiness", 0);
        set => PlayerPrefs.SetInt("Happiness", value);
    }

    public int PlayerMoney
    {
        get => PlayerPrefs.GetInt("Money", 0);
        set => PlayerPrefs.SetInt("Money", value);
    }

    public int PlayerNetWorth
    {
        get => PlayerPrefs.GetInt("NetWorth", 0);
        set => PlayerPrefs.SetInt("NetWorth", value);
    }
}
