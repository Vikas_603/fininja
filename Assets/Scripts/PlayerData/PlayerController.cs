using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    public Animator anim;
    public const string animParameter = "isWalking";
    public  bool isWalking;
    [SerializeField] private float moveSpeed = 2f;
    [SerializeField] private WayPointsManager wayPointsManager;
    public int playerlastIndex = 0;
    private int steps = 0;
    public GameManager gameManager;
    public CardManager cardManager;
    public RoadSpawner roadSpawner;

    public static Action OnPlayerStoped;
    public static Action<int> OnPlayerStopedAtInsurence;
    private void Start()
    {
        isWalking = false;
    }

    private void OnEnable()
    {
        GameManager.OnPlayerMovement += GetSteps;
    }

    private void OnDisable()
    {
        GameManager.OnPlayerMovement -= GetSteps;
    }

    private void GetSteps(int Steps)
    {
        steps = Steps;
        isWalking = true;
    }
    
    public void Update()
    {
        anim.SetBool(animParameter, isWalking);
        Move();
    }

    void Move()
    {
        if (transform.position.z == wayPointsManager.wayPoints[wayPointsManager.wayPoints.Count-1].transform.position.z)
        {
            roadSpawner.SpawnRoad();
            cardManager.UpdateCards();

        }
        if (isWalking)
        {
            transform.position = Vector3.MoveTowards(transform.position, wayPointsManager.wayPoints[wayPointsManager.wayPointIndex].transform.position, moveSpeed * Time.deltaTime);
            transform.LookAt(wayPointsManager.wayPoints[wayPointsManager.wayPointIndex]);
            
            if (transform.position == wayPointsManager.wayPoints[wayPointsManager.wayPointIndex].transform.position)
            {
                if (1 + steps > wayPointsManager.wayPointIndex)
                {
                    wayPointsManager.wayPointIndex++;
                }
                else
                {
                    isWalking = false;
                    gameManager.CheckFixedEvents();
                    gameManager.UpdatePlayerMoney();

                    //get cards ID Here
                    int current_CardId = wayPointsManager.wayPoints[wayPointsManager.wayPointIndex].transform.GetComponent<CardIDHolder>().MyCardID;

                    //Destroy Previous Road
                    for (int i = wayPointsManager.wayPoints.Count-1; i >=0 ; i--)
                    {
                        if (wayPointsManager.wayPoints[i].position.z < transform.position.z-5)
                        {
                            GameObject temp = wayPointsManager.wayPoints[i].gameObject;
                            wayPointsManager.wayPoints.Remove(wayPointsManager.wayPoints[i]);
                            Destroy(temp);
                            wayPointsManager.wayPointIndex--;
                        }
                    }
                    // generate cards Here
                    if (current_CardId != 6)
                    {
                        gameManager.GenerateCard(current_CardId, cardManager.cards[current_CardId].index,
                            cardManager.cards[current_CardId].subIndex, cardManager.cards[current_CardId].sub_subIndex);
                    }
                    else
                    {
                        if (GameManager.isLifeInsuranceActive && !GameManager.isHealthInsurence)
                        {
                            OnPlayerStopedAtInsurence?.Invoke(0);
                        }
                        else if (GameManager.isHealthInsurence && !GameManager.isLifeInsuranceActive)
                        {
                            OnPlayerStopedAtInsurence?.Invoke(1);
                        }
                        else
                        {
                            OnPlayerStopedAtInsurence?.Invoke(Random.Range(0,2));
                        }
                        
                    }
                }
            }
            
        }
    }
}