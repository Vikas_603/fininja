using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public Transform player;
   [SerializeField] private float X_Offset = 3.37f;
   [SerializeField]private float Y_Offset = 2.33f;
   [SerializeField]private float Z_Offset = -7.06f;

    private void LateUpdate()
    {
        transform.position = new Vector3(player.transform.position.x + X_Offset, player.transform.position.y + Y_Offset,
            player.transform.position.z + Z_Offset);
    }
}
