using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Sirenix.OdinInspector;
using UnityEngine;

public class CardManager : MonoBehaviour
{
    public CardsData AllCards;
    public GameManager gameManager;
public CardsFiltered[] cards;
    List<float> tempList = new List<float>();
    float large = 0;
    
    private void Start()
    {
        
        UpdateCards();
    }

    public void UpdateCards()
    {
        for (int i = 0; i < AllCards.cards.Length; i++)
        {
            cards[i].CardID = i;
            cards[i].cardCategoryName = AllCards.cards[i].cat.ToString();

            for (int j = 0; j < AllCards.cards[i].subCat.Length; j++)
            {
                for (int k = 0; k < AllCards.cards[i].subCat[j].subsubCat.Length; k++)
                {

                    tempList.Add(AllCards.cards[i].subCat[j].subsubCat[k].value);
                }
            }
            LargestFilter(tempList);
            tempList.Clear();
            SearchInCategories();
        }
    }
    
  
    #region helperFunction
    
    private float LargestFilter(List<float> ListToCompare)
    {
        int i = 0;

        large = ListToCompare[0];

        for (i = 1; i < ListToCompare.Count; i++)
        {
            if (large < ListToCompare[i])
                large = ListToCompare[i];
        }

        return large;
    }
    
    private void SearchInCategories()
    {
        for (int i = 0; i < AllCards.cards.Length; i++)
        {
            for (int j = 0; j < AllCards.cards[i].subCat.Length; j++)
            {
                for (int k = 0; k < AllCards.cards[i].subCat[j].subsubCat.Length; k++)
                {
                    if (AllCards.cards[i].subCat[j].subsubCat[k].value == large)
                    {
                        cards[i].cardSubCategory = AllCards.cards[i].subCat[j].subCat.ToString();
                        cards[i].cardSubSubCategory = AllCards.cards[i].subCat[j].subsubCat[k].subsubcat.ToString();
                        cards[i].cost = AllCards.cards[i].subCat[j].subsubCat[k].Cost;
                        cards[i].index = i;
                        cards[i].subIndex = j;
                        cards[i].sub_subIndex = k;
                    }
                }
            }
        }
    }
    #endregion

    #region Inventry

    public GameObject inventryPanel;
    public GameObject inventryItemPrefab;
    public GameObject content;
    public void OpenInventry()
    {
        for (int i = 0; i < AllCards.cards.Length; i++)
        {
            for (int j = 0; j < AllCards.cards[i].subCat.Length; j++)
            {
                for (int k = 0; k < AllCards.cards[i].subCat[j].subsubCat.Length; k++)
                {
                    if (AllCards.cards[i].subCat[j].subsubCat[k].PurchaseCounts > 1)
                    {
                        InventryItem item = Instantiate(inventryItemPrefab, content.transform)
                            .GetComponent<InventryItem>();
                        item.SubSubCategory.text = AllCards.cards[i].subCat[j].subsubCat[k].subsubcat.ToString();
                        item.subCategory.text = AllCards.cards[i].subCat[j].subCat.ToString();
                        item.itemCount.text = (AllCards.cards[i].subCat[j].subsubCat[k].PurchaseCounts-1).ToString();
                    }
                }
            }
        }

        if (GameManager.isHealthInsurence)
        {
            InventryItem item = Instantiate(inventryItemPrefab, content.transform)
                .GetComponent<InventryItem>();
            item.subCategory.text = "Health";
            item.SubSubCategory.text = "Insurence";
            item.itemCount.text = "10,000";
        }

        if (GameManager.isLifeInsuranceActive)
        {
            InventryItem item = Instantiate(inventryItemPrefab, content.transform)
                .GetComponent<InventryItem>();
            item.subCategory.text = "Life";
            item.SubSubCategory.text = "Insurence";
            item.itemCount.text = gameManager.lifeInsurenceCostValue.ToString();
        }
        inventryPanel.SetActive(true);
    }

    public void CloseInventry()
    {
        inventryPanel.SetActive(false);
        if (content.transform.childCount > 0)
        {
            for (int i = 0; i < content.transform.childCount; i++)
            {
                Destroy(content.transform.GetChild(i).gameObject);
            }
        }
    }
    #endregion
}

#region Structure

[Serializable]
public class CardsFiltered
{
    public int CardID;
    public string cardCategoryName;
    public string cardSubCategory;
    public string cardSubSubCategory;
    public int cost;
    public int index;
    public int subIndex;
    public int sub_subIndex;
}

#endregion

