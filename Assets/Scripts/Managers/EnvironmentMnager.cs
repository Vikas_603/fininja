using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentMnager : MonoBehaviour
{
    public GameObject environmentPrefab;
    public int offset = 30;

    public static int lastSpawnPoint = 105;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            SpawnEnvironment();
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(WaitAndDestroy());
        }
    }

    private void SpawnEnvironment()
    {
        GameObject newEnvironment = Instantiate(environmentPrefab);
        newEnvironment.transform.position = new Vector3(0.8560295f,5.516503f,lastSpawnPoint + offset);
        lastSpawnPoint = lastSpawnPoint + offset;
    }

    IEnumerator WaitAndDestroy()
    {
        yield return new WaitForSeconds(10f);
        Destroy(gameObject.transform.parent.gameObject);
    }
    
}
