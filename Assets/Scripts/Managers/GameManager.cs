using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Sirenix.OdinInspector;

public class GameManager : MonoBehaviour
{
    public static Action<int> OnPlayerMovement;
    [SerializeField] private TextMeshProUGUI diceTxt;
    
    public int CellsCounter;
    
    public int playerMoney = 10000;

    public TextMeshProUGUI playerMoneyTxt;

    public Button diceObject;
    public GameObject popUp;
    public TextMeshProUGUI popupText;

    public CardManager cardManager;
    public GameObject card;
    
    public TextMeshProUGUI card_catNameTxt;
    public TextMeshProUGUI card_subcatNameTxt;
    public TextMeshProUGUI card_sub_subcatNameTxt;
    public TextMeshProUGUI card_CostTxt;
    public TextMeshProUGUI card_CostPaidInsurenceTxt;
    
    private int currentItemCost;
    private int Current_cardId;
    private int card_Index;
    private int card_SubIndex;
    private int card_SubSubIndex;
    
    private void Start()
    {
        isHealthInsurence = false;
        isLifeInsuranceActive = false;
        UpdatePlayerMoney();
    }
    public void UpdatePlayerMoney()
    {
        playerMoneyTxt.text = playerMoney.ToString();
    }

    public CardsData Allcards;
    #region FixedEvents Variables
    
    private int playerSalaryAmount = 25000;
    private int taxAmount = 6000;
    private int billAmount = 25000;
    private int jackpotAmount = 2000;

    private int salaryCellCounter;
    private int billCellCounter;
    private int taxCellCounter;
    private int InsurenceCellCounter;
    private int JackpotCellCounter;
    
    private int salaryCell = 30;
    private int billCell = 33;
    private int taxCell = 120;
    private int InsurenceCell = 115;
    private int JackpotCell = 110;
    public static bool isHealthInsurence;
    private int insurenceAmount = 10000;
    #endregion
    
    public void DiceThrow()
    {
        int diceNumber = Random.Range(1, 7);
        diceTxt.text = diceNumber.ToString();
        OnPlayerMovement?.Invoke(diceNumber);
        CellsCounter += diceNumber;
        salaryCellCounter += diceNumber;
        billCellCounter += diceNumber;
        taxCellCounter += diceNumber;
        InsurenceCellCounter += diceNumber;
        JackpotCellCounter += diceNumber;
        diceObject.interactable = false;
    }

    public GameObject insurenceCard;
    public GameObject lifeInsurenceCard;
    public TextMeshProUGUI lifeInsurenceCost;
    public int lifeInsurenceCostValue;
    public static bool isLifeInsuranceActive;
    
    private void OnEnable()
    {
        PlayerController.OnPlayerStopedAtInsurence += ShowInsurenceCard;
    }

    private void OnDisable()
    {
        PlayerController.OnPlayerStopedAtInsurence -= ShowInsurenceCard;
    }
    
    private void ShowInsurenceCard(int data)
    {
        if (data == 0)
        {
            insurenceCard.SetActive(true);
        }
        else
        {
            lifeInsurenceCard.SetActive(true);
            if(playerMoney >=15000)
            {
                lifeInsurenceCost.text = "Insurance Cost = 15,000";
                lifeInsurenceCostValue = 15000;
            }
            else
            {
                lifeInsurenceCost.text = "Insurance Cost = 10,000";
                lifeInsurenceCostValue = 10000;
            }
        }
    }
    
    public void CheckFixedEvents()
    {
        if (salaryCellCounter >= salaryCell)
        {
            playerMoney += playerSalaryAmount;
            salaryCellCounter = 0;
            popUp.SetActive(true);
            popupText.text = "Your Salary Credited :-" + playerSalaryAmount;
            StartCoroutine(WaitAndClosePopUp());
        }
        else if(billCellCounter >= billCell)
        {
            playerMoney -= billAmount;
            billCellCounter = 0;
            popUp.SetActive(true);
            popupText.text = "Your Bill/Rent Debited :-" + billAmount;
            StartCoroutine(WaitAndClosePopUp());
        }
        else if (taxCellCounter >= taxCell)
        {
            playerMoney -= taxAmount;
            taxCellCounter = 0;
            popUp.SetActive(true);
            popupText.text = "Your Tax Debited :-" + taxAmount;
            StartCoroutine(WaitAndClosePopUp());
        }
        else if (InsurenceCellCounter >= InsurenceCell)
        {
            playerMoney -= insurenceAmount;
            InsurenceCellCounter = 0;
            popUp.SetActive(true);
            popupText.text = "Your Insurence Debited :-" + taxAmount;
            StartCoroutine(WaitAndClosePopUp());
        }
        else if(JackpotCellCounter >= JackpotCell)
        {
            playerMoney += jackpotAmount;
            JackpotCellCounter = 0;
            popUp.SetActive(true);
            popupText.text = "Your JackPot Credited :-" + jackpotAmount;
            StartCoroutine(WaitAndClosePopUp());
        }
        UpdatePlayerMoney();
    }

    private IEnumerator WaitAndClosePopUp()
    {
        yield return new WaitForSeconds(2f);
        popUp.SetActive(false);
    }

    public void GenerateCard(int CardID , int Index , int SubIndex, int Sub_SubIndex)
    {
        Current_cardId = CardID;
        card_Index = Index;
        card_SubIndex = SubIndex;
        card_SubSubIndex = Sub_SubIndex;

        card_catNameTxt.text = cardManager.cards[CardID].cardCategoryName;
        card_subcatNameTxt.text = cardManager.cards[CardID].cardSubCategory;
        card_sub_subcatNameTxt.text = cardManager.cards[CardID].cardSubSubCategory;
        card_CostPaidInsurenceTxt.text = "";
        if (CardID == 5 && (isHealthInsurence || isLifeInsuranceActive))
        {
            card_CostTxt.text = "Total Bill:- " +cardManager.cards[CardID].cost +" "+ "Payable:- " +(cardManager.cards[CardID].cost * 20 / 100);
            currentItemCost = cardManager.cards[CardID].cost * 20 / 100;
            card_CostPaidInsurenceTxt.text =
                "Amount Paid By Insurence :- " + (cardManager.cards[CardID].cost - currentItemCost);
            popUp.SetActive(true);
            popupText.text = "Your Remaining Amount Paid by Insurance";
            StartCoroutine(WaitAndClosePopUp());
        }
        else
        {
            card_CostTxt.text = "Amount:- "+cardManager.cards[CardID].cost.ToString();
            currentItemCost = cardManager.cards[CardID].cost;
        }
        card.SetActive(true);
    }

    public void OnBuyHealthInsurence()
    {
        if (playerMoney >= 100)
        {
            playerMoney -= 100;
            UpdatePlayerMoney();
            Debug.Log("Item Buy Successfully");
            isHealthInsurence = true;
        }
        else
        {
            popUp.SetActive(true);
            popupText.text = "Not Enough Money! Try Again Later";
            Debug.Log("Not Enough Money for this Item");
        }
        
        insurenceCard.SetActive(false);
        // Show Dice Here again
        diceObject.interactable = true;
    }

    public void OnBuyLifeInsurence()
    {
        if (playerMoney >= 50)
        {
            playerMoney -= 50;
            UpdatePlayerMoney();
            isLifeInsuranceActive = true;
        }
        else
        {
            popUp.SetActive(true);
            popupText.text = "Not Enough Money! Try Again Later";
        }

        lifeInsurenceCard.SetActive(false);
        diceObject.interactable = true;

    }
    public void On_ClickPay()
    {
        if (playerMoney >= currentItemCost)
        {
            playerMoney -= currentItemCost;
            UpdatePlayerMoney();
            Debug.Log("Item Buy Successfully");
            Allcards.cards[card_Index].subCat[card_SubIndex].subsubCat[card_SubSubIndex].PurchaseCounts += 1;
            Allcards.cards[card_Index].subCat[card_SubIndex].subsubCat[card_SubSubIndex].value = (1 / 3) * (1 / 2) *
                (1 / Allcards.cards[card_Index].subCat[card_SubIndex].subsubCat[card_SubSubIndex].PurchaseCounts);
        }
        else
        {
            Debug.Log("Not Enough Money for this Item");
        }
        
        card.SetActive(false);
        // Show Dice Here again
        diceObject.interactable = true;
    }

    public void Reset_buttonCLicked()
    {
        Allcards.ResetData();
    }
}

