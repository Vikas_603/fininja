using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoadSpawner : MonoBehaviour
{
    public WayPointsManager waypoints;
    public int roadToSpawnCount;
   
    public GameObject RoadPrefab;
    public GameObject RoadParent;
    public float ForwardoffsetZ = 3f;
    public float currentRotY = 0f;
    public float MinRotAngle = -18f;
    public float MaxRotAngle = 18f;
    private int roadLength = 0;


    private void Start()
    {
        SpawnRoad();
    }
    
    public void SpawnRoad()
    {

        for (int i = 0; i < roadToSpawnCount; i++)
        {
           GameObject temp = Instantiate(RoadPrefab,RoadParent.transform);
           temp.transform.localPosition = new Vector3(0, 0, ForwardoffsetZ*(i+roadLength));

           temp.GetComponent<CardIDHolder>().MyCardID = i;
           temp.GetComponent<CardIDHolder>().objects[i].SetActive(true);
           
           temp.transform.localEulerAngles = new Vector3(0, Random.Range(MinRotAngle,MaxRotAngle), 0f);
           waypoints.wayPoints.Add(temp.transform);

        }

        roadLength += roadToSpawnCount;
    }
}
