using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InventryItem : MonoBehaviour
{
    public TextMeshProUGUI SubSubCategory;
    public TextMeshProUGUI subCategory;
    public TextMeshProUGUI itemCount;
}
