using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


[CreateAssetMenu(fileName = "CardsData", menuName = "ScriptableObjects/Cards")]
public class CardsData : ScriptableObject
{
    public CardsData backupData;
    public enum Category
    {
        Malls,
        Food,
        Entertainment,
        Vacation,
        Transport,
        Health,
        MedicalEmergency,
        Insurance,
        Investment,
        Parents
    }

    public enum SubCategory
    {
        Clothes,
        Shoes,
        Electronics,
        GetGroceries,
        EatOutside,
        Cinema,
        OTT,
        Live,
        Solo,
        WithFriends,
        Public,
        Private,
        Exercise,
        Checkup,
        Accident,
        Sick,
        Automobile,
        Health,
        BringParents,
        VisitParents,
        Null
    }
    
    public enum SubSubCategory
    {
        Formals,
        Casuals,
        Laptop,
        Phone,
        Watch,
        TwoD,
        ThreeD,
        IMAX,
        UmazonCo_Prime,
        Setflix,
        CoolStar,
        StandUpComedy,
        Concert,
        FoodFest,
        WeekendGetaway,
        WeekLong,
        Bus,
        Metro,
        Cab,
        BuyCar,
        BuyBike,
        VechicleService,
        GetGymMembership,
        Jogging,
        GetYogaClassMembership,
        Self,
        GovernmentCamp,
        Clinic,
        GovernmentHospital,
        PrivateHospital,
        Car,
        Bike,
        Life,
        Term,
        Medical,
        BringParents,
        VisitParents,
        Null
    }

    public Categories[] cards;
    

    [Serializable]
    public class Categories
    {
        public Category cat;
        public Subcategories[] subCat;
    }
    [Serializable]
    public class Subcategories
    {
        public SubCategory subCat;
        public SubSubcategories[] subsubCat;
    }

    [Serializable]
    public class SubSubcategories
    {
        public SubSubCategory subsubcat;
        public int Cost;
        public int PurchaseCounts;
        public float value;
    }
    
   
#if UNITY_EDITOR
    
    [Button]
    void saveData(){
        //EditorUtility.SetDirty();
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
#endif
    [Button]
    public void ResetData()
    {
        for (int i = 0; i < cards.Length; i++)
        {
            for (int j = 0; j < cards[i].subCat.Length; j++)
            {
                for (int k = 0; k < cards[i].subCat[j].subsubCat.Length; k++)
                {
                    cards[i].subCat[j].subsubCat[k].PurchaseCounts =
                        backupData.cards[i].subCat[j].subsubCat[k].PurchaseCounts;

                    cards[i].subCat[j].subsubCat[k].value = backupData.cards[i].subCat[j].subsubCat[k].value;
                }
            }
        }
    }
}